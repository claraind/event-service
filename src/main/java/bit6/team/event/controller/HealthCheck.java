package bit6.team.event.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import bit5.team2.library.base.BaseController;
import bit5.team2.library.output.ResultEntity;

@RestController
public class HealthCheck extends BaseController {
    @GetMapping("/event/")
    public ResultEntity<Object> success1() {
        return this.success(null);
    }

    @GetMapping("/verification/")
    public ResultEntity<Object> success2() {
        return this.success(null);
    }
}
