package bit6.team.event.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import bit5.team2.library.entity.Event;

@Repository
public interface EventRepo extends PagingAndSortingRepository<Event, String> {

}
